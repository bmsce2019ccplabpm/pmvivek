#include <stdio.h>


int getinput()
{
	int a;
	printf("Enter your height\n");
	scanf("%d",&a);
	return a;
}


int inputage()
{
	int a;
	printf("Enter your age\n");
	scanf("%d",&a);
	return a;
}


char inputc()
{
	char ch;
	printf("Enter your section\n");
	scanf("%s",&ch);
	return ch;
}

void output(int a,int b,char c[3])
{
	printf("Your height is %d\n",a);
	printf("Your age is %d\n",b);
	printf("Your section is %s\n",c);
}


int main()
{
int a=getinput();
int b=inputage();
char c=inputc();
output(a,b,&c);
return 0;
}