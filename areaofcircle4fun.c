#include<stdio.h>
#include<math.h>
float get_input()
{
	float x;
    printf("enter the radius\n");
    scanf("%f",&x);
    return x;
}
float area(float y)
{
	float area1;
    area1 = 3.14*y*y;
    return area1;
}
void output(float a, float total)
{
    printf("area of circle %f and %f\n",a, total);
}
int main ()
{
    float r,total;
    r=get_input();
    total=area(r);
    output(r, total);
    return 0;
}